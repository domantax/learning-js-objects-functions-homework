var searchModule = (function() {
  var _input, _output, _value;

  function _updateOutput(event) {
    _value = _input.value;
    console.log(_value);
    _output.innerHTML = _value;
  }

  function init(querySelectorInput, querySelectorOutput) {
    _input = document.querySelector(querySelectorInput);
    _output = document.querySelector(querySelectorOutput);
    console.log('running init', _input, _output);
    _input.addEventListener('keyup', _updateOutput);
  }

  return {
    initSearchModule: init,
  };
})();

searchModule.initSearchModule('.input', '.message');
